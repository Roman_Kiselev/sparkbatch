package ru.epam.spark;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.datediff;
import static org.apache.spark.sql.functions.from_json;
import static org.apache.spark.sql.functions.lag;
import static org.apache.spark.sql.functions.year;
import static ru.epam.spark.AppConstant.BATCHING_LIMIT;
import static ru.epam.spark.AppConstant.HOTEL_ID;
import static ru.epam.spark.AppConstant.ID;
import static ru.epam.spark.AppConstant.IDLE;
import static ru.epam.spark.AppConstant.KEY;
import static ru.epam.spark.AppConstant.OFFSET;
import static ru.epam.spark.AppConstant.SRCH_CI;
import static ru.epam.spark.AppConstant.STRING;
import static ru.epam.spark.AppConstant.VALUE;
import static ru.epam.spark.AppConstant.YEAR;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import lombok.extern.log4j.Log4j;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import ru.epam.spark.model.Hotel;

@Log4j
public class Application {
    public static void main(String[] args) {
        Map<String, Object> props = readProps();
        SparkSession spark = SparkSession
                .builder()
                .master("local[*]")
                .appName("spark batch")
                .getOrCreate();
        ObjectMapper mapper = new ObjectMapper();
        Dataset<Row> kafka = spark.read()
                .format("kafka")
                .option("kafka.bootstrap.servers", "localhost:9092")
                .option("subscribe", "aggregateSingleTopic")
                .option("startingOffsets", (String) props.getOrDefault(OFFSET, "earliest"))
                .option("maxOffsetsPerTrigger", (long) props.getOrDefault(BATCHING_LIMIT, 100))
                .load();
        kafka.select(
                col(KEY).cast(STRING),
                col(VALUE).cast(STRING),
                from_json(col(KEY).cast(STRING), getKeySchema()))
                .show(false);
        JavaRDD<Hotel> rdd = kafka.select(
                col(VALUE).cast(STRING)
        ).toJavaRDD()
                .map(it -> mapper.readValue(it.getString(0), Hotel.class));
        Dataset<Hotel> hotelDataSet = spark.createDataset(rdd.rdd(), Encoders.bean(Hotel.class));
        hotelDataSet.show(false);
        StructType structType = new StructType();
        structType.add(StructField.apply(ID, DataTypes.LongType, true, Metadata.empty()));
        structType.add(StructField.apply("date_time", DataTypes.DateType, true, Metadata.empty()));
        Dataset<Row> avro = spark
                .read()
                .format("avro")
                .load("hdfs://localhost:9000//tmp/dataset/expedia/part-*");
        avro.printSchema();
        Dataset<Row> typed = avro.select(getColumns());
        WindowSpec window = Window.partitionBy(HOTEL_ID).orderBy(SRCH_CI);
        Dataset<Row> idle = typed.withColumn("prevCheckIn",
                lag("srch_co", 1, null).over(window))
                .withColumn(IDLE, datediff(col(SRCH_CI), col("prevCheckIn")));
        Dataset<Row> invalidIdle = idle.filter(col(IDLE).isNotNull())
                .filter(col(IDLE).gt(1))
                .filter(col(IDLE).lt(30));
        invalidIdle.join(hotelDataSet, invalidIdle.col(HOTEL_ID).equalTo(hotelDataSet.col(ID)))
                .show(false);
        Dataset<Row> validIdle = idle.filter(col(IDLE).lt(2).or(col(IDLE).gt(30)));
        validIdle.groupBy(HOTEL_ID).count().show();
        validIdle.join(hotelDataSet, validIdle.col(HOTEL_ID).equalTo(hotelDataSet.col(ID)))
                .groupBy("city")
                .count().show();
        validIdle.withColumn(YEAR, year(col(SRCH_CI))).write().partitionBy(YEAR)
                .save("hdfs://localhost:9000//tmp/newDataMaster");

        spark.cloneSession();
    }

    private static StructType getKeySchema() {
        StructType structType = new StructType();
        structType.add(StructField.apply("geoHash", DataTypes.StringType, true, Metadata.empty()));
        structType.add(StructField.apply("date", DataTypes.DateType, true, Metadata.empty()));
        return structType;
    }
    
    private static Column[] getColumns() {
        return new Column[] {
                col(ID).cast(DataTypes.LongType),
                col("date_time").cast(DataTypes.DateType),
                col("site_name").cast(DataTypes.IntegerType),
                col("posa_continent").cast(DataTypes.IntegerType),
                col("user_location_country").cast(DataTypes.IntegerType),
                col("user_location_region").cast(DataTypes.IntegerType),
                col("user_location_city").cast(DataTypes.IntegerType),
                col("orig_destination_distance").cast(DataTypes.DoubleType),
                col("user_id").cast(DataTypes.IntegerType),
                col("is_mobile").cast(DataTypes.IntegerType),
                col("is_package").cast(DataTypes.IntegerType),
                col("channel").cast(DataTypes.IntegerType),
                col(SRCH_CI).cast(DataTypes.DateType),
                col("srch_co").cast(DataTypes.DateType),
                col("srch_adults_cnt").cast(DataTypes.IntegerType),
                col("srch_children_cnt").cast(DataTypes.IntegerType),
                col("srch_rm_cnt").cast(DataTypes.IntegerType),
                col("srch_destination_id").cast(DataTypes.IntegerType),
                col("srch_destination_type_id").cast(DataTypes.IntegerType),
                col(HOTEL_ID).cast(DataTypes.LongType)
        };
    }

    private static Map<String, Object> readProps() {
        try (InputStream props = Application.class.getClassLoader().getResourceAsStream("app.properties")) {
            Properties prop = new Properties();
            prop.load(props);
            Map<String, Object> result = new HashMap<>();
            result.put(OFFSET, prop.get(OFFSET));
            result.put(BATCHING_LIMIT, Long.parseLong((String) prop.get(BATCHING_LIMIT)));
            return  result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return new HashMap<>();
    }
}

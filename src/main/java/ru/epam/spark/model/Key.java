package ru.epam.spark.model;

import java.util.Date;
import lombok.Data;

@Data
public class Key {

    private String geoHash;
    private Date date;
}

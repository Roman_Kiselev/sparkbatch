package ru.epam.spark.model;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class Hotel {
    private Long id;
    private String name;
    private String country;
    private String city;
    private String address;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private String geoHash;
    private String date;
    private BigDecimal temperature;
}

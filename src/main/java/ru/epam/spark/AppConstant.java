package ru.epam.spark;

public class AppConstant {
    public static final String STRING = "string";
    public static final String KEY = "key";
    public static final String VALUE = "value";
    public static final String ID = "id";
    public static final String HOTEL_ID = "hotel_id";
    public static final String SRCH_CI = "srch_ci";
    public static final String YEAR = "year";
    public static final String IDLE = "idle";
    public static final String BATCHING_LIMIT = "batching.limit";
    public static final String OFFSET = "offset";
}
